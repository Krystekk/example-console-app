#import <Foundation/Foundation.h>
#import "VRectangle.h"
#import "VCircle.h"
#import "VScreen.h"
#import "Categories/NSArray+XML.h"

NSString *const V_SAMPLE_SCREEN_NAME = @"Sample";

int main(int argc, const char *argv[]) {
    @autoreleasepool {

        VScreen *sampleScreen = [[VScreen alloc] initWithScreenName:V_SAMPLE_SCREEN_NAME
                                                             shapes:@[
                [[VCircle alloc] initWithRadius:50.0f point:CGPointMake(100.0f, 100.0f)],
                [[VCircle alloc] initWithRadius:20.0f point:CGPointMake(50.0f, 50.0f)],
                [[VCircle alloc] initWithRadius:20.0f point:CGPointMake(150.0f, 150.0f)],
                [[VRectangle alloc] initWithSize:CGSizeMake(40, 15) point:CGPointMake(100, 110)],
                [[VRectangle alloc] initWithSize:CGSizeMake(10, 40) point:CGPointMake(100, 90)],
        ]];
        NSLog(@"\n%@",@[sampleScreen].printShapesXML);

        [sampleScreen moveContent:CGPointMake(10, 15)];
        NSLog(@"\n%@",@[sampleScreen].printShapesXML);

        //AFTER moveContent
        NSLog(@"countAreasOfShapesInScreen: %.02f", sampleScreen.countAreasOfShapesInScreen);

        [sampleScreen makeBoundingBox:^(CGFloat minX, CGFloat maxX, CGFloat minY, CGFloat maxY) {
            NSLog(@"makeBoundingBox minX: %.02f maxX: %.02f minY: %.02f maxY: %.02f", minX, maxX, minY, maxY);
            NSLog(@"Box should have width %.02f and height %.02f", maxX - minX, maxY - minY);
        }];
    }
    return 0;
}
#import "VXMLProtocol.h"
#import "VScreen.h"
#import "Categories/NSString+XML.h"

static NSString *const V_SCREEN_XML_FORMAT_FIRST_LINE = @"\t<screen name=\"%@\">";
static NSString *const V_SCREEN_XML_FORMAT_LAST_LINE = @"\t</screen>";

@implementation VScreen

- (instancetype)initWithScreenName:(NSString *)screenName shapes:(NSArray <VShape *> *)shapes {
    self = [super init];
    if (self) {
        _screenName = screenName;
        _shapes = shapes;
    }

    return self;
}

- (NSString *)prepareXml {
    NSString *xml = [NSString stringWithFormat:V_SCREEN_XML_FORMAT_FIRST_LINE, self.screenName];
    for (id object in self.shapes) {
        if ([self isShape:object]) {
            id <VXMLProtocol> xmlProtocol = object;
            xml = [xml breakLine];
            xml = [xml stringByAppendingString:xmlProtocol.prepareXML];
        }
    }
    xml = [xml breakLine];
    return [xml stringByAppendingString:V_SCREEN_XML_FORMAT_LAST_LINE];
}

- (void)moveContent:(CGPoint)point {
    for (id object in self.shapes) {
        if ([self isShape:object]) {
            VShape *shape = (VShape *) object;
            [shape moveWithPoint:point];
        }
    }
}

- (CGFloat)countAreasOfShapesInScreen {
    CGFloat areas = 0.0f;
    for (id object in self.shapes) {
        if ([self isShape:object]) {
            VShape *shape = (VShape *) object;
            areas += shape.area;
        }
    }

    return areas;
}

- (void)makeBoundingBox:(void(^)(CGFloat minX, CGFloat maxX, CGFloat minY, CGFloat maxY))completion {
    CGFloat minX = CGFLOAT_MAX;
    CGFloat maxX = 0.0f;
    CGFloat minY = CGFLOAT_MAX;
    CGFloat maxY = 0.0f;

    for (id object in self.shapes) {
        if ([self isShape:object]) {
            VShape *shape = (VShape *) object;
            minX = shape.point.x < minX ? shape.point.x : minX;
            minY = shape.point.y < minY ? shape.point.y : minY;
            maxX = (shape.point.x + shape.shapeSize.width) > maxX ? (shape.point.x + shape.shapeSize.width) : maxX;
            maxY = (shape.point.y + shape.shapeSize.height) > maxY ? (shape.point.x + shape.shapeSize.height) : maxY;
        }
    }

    completion(minX, maxX, minY, maxY);
}

- (BOOL)isShape:(id)object {
    return [object isKindOfClass:[VShape class]];
}

@end

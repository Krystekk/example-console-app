#import <Foundation/Foundation.h>

@protocol VXMLProtocol <NSObject>

- (NSString *)prepareXML;

@end

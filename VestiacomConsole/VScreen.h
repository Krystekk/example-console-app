#import <Foundation/Foundation.h>
#import "VShape.h"

@interface VScreen : NSObject

@property(nonatomic, strong, readonly) NSArray <VShape *> *shapes;
@property(nonatomic, strong, readonly) NSString *screenName;

- (instancetype)initWithScreenName:(NSString *)screenName shapes:(NSArray <VShape *> *)shapes;

- (void)moveContent:(CGPoint)point;

- (CGFloat)countAreasOfShapesInScreen;

- (NSString *)prepareXml;

- (void)makeBoundingBox:(void(^)(CGFloat minX, CGFloat maxX, CGFloat minY, CGFloat maxY))completion;

@end
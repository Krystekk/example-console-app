#import "NSArray+XML.h"
#import "VScreen.h"
#import "NSString+XML.h"

static NSString *const V_SCREENS_XML_FORMAT_FIRST_LINE = @"<screens>";
static NSString *const V_SCREENS_XML_FORMAT_LAST_LINE = @"</screens>";

@implementation NSArray (XML)

- (NSString *)printShapesXML {
    NSString *xml = V_SCREENS_XML_FORMAT_FIRST_LINE;
    xml = [xml breakLine];
    for (id object in self) {
        if ([self checkKindOfClassObject:object]) {
            xml = [xml stringByAppendingString:((VScreen *) object).prepareXml];
        }
    }

    xml = [xml breakLine];
    return [xml stringByAppendingString:V_SCREENS_XML_FORMAT_LAST_LINE];
}

- (BOOL)checkKindOfClassObject:(id)object {
    return [object isKindOfClass:[VScreen class]];
}

@end
#import "NSString+XML.h"


@implementation NSString (XML)

- (NSString *)breakLine {
    return [self stringByAppendingString:@"\n"];
}

@end
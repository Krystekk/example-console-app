#import <Foundation/Foundation.h>

@interface NSString (XML)

- (NSString *)breakLine;

@end
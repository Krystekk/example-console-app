#import <Foundation/Foundation.h>

@interface NSArray (XML)

- (NSString *)printShapesXML;

@end
#import "VCircle.h"

static NSString *const XML_CIRCLE_FORMAT = @"\t\t<circle x=\"%.02f\" y=\"%.02f\" radius=\"%.02f\"/>";

@implementation VCircle

- (instancetype)initWithRadius:(CGFloat)radius point:(CGPoint)point {
    self = [super initWithPoint:point];
    if (self) {
        _radius = radius;
    }

    return self;
}

- (NSString *)prepareXML {
    return [NSString stringWithFormat:XML_CIRCLE_FORMAT, self.point.x, self.point.y, self.radius];
}

- (CGFloat)area {
    return M_PI * (_radius * 2);
}

- (CGSize)shapeSize {
    return CGSizeMake((_radius * 2), (_radius * 2));
}

@end

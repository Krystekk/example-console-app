#import <Foundation/Foundation.h>
#import "VXMLProtocol.h"

@interface VShape : NSObject <VXMLProtocol>

@property(nonatomic, assign, readonly) CGPoint point;

- (instancetype)initWithPoint:(CGPoint)point;

- (void)moveWithPoint:(CGPoint)point;

- (CGFloat)area;

- (CGSize)shapeSize;

@end

#import "VShape.h"

@interface VCircle : VShape

@property(nonatomic, assign, readonly) CGFloat radius;

- (instancetype)initWithRadius:(CGFloat)radius point:(CGPoint)point;

@end
#import "VShape.h"

@interface VRectangle : VShape

@property(nonatomic, assign, readonly) CGSize size;

- (instancetype)initWithSize:(CGSize )size point:(CGPoint )point;

@end
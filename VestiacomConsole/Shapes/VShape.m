#import "VShape.h"

@implementation VShape

- (instancetype)initWithPoint:(CGPoint)point {
    self = [super init];
    if (self) {
        _point = point;
    }
    
    return self;
}

- (NSString *)prepareXML {
    NSString *errorText = @"Called %@ on superclass VShape.Override this protocol when you create new shape";
    @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:[NSString stringWithFormat:errorText, NSStringFromSelector(_cmd)] userInfo:@{@"object": self}];
}

- (void)moveWithPoint:(CGPoint)point {
    _point = CGPointMake(_point.x + point.x, _point.y + point.y);
}

- (CGFloat)area {
    NSString *errorText = @"Called %@ on superclass VShape.Override this methods when you create new shape";
    @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:[NSString stringWithFormat:errorText, NSStringFromSelector(_cmd)] userInfo:@{@"object": self}];
}

- (CGSize)shapeSize {
    NSString *errorText = @"Called %@ on superclass VShape.Override this methods when you create new shape";
    @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:[NSString stringWithFormat:errorText, NSStringFromSelector(_cmd)] userInfo:@{@"object": self}];
}

@end

#import "VRectangle.h"

static NSString *const XML_RECTANGLE_FORMAT = @"\t\t<rectangle x=\"%.02f\" y=\"%.02f\" shapeSize=\"%.02f\" height=\"%.02f\"/>";

@implementation VRectangle

- (instancetype)initWithSize:(CGSize )size point:(CGPoint )point {
    self = [super initWithPoint:point];
    if (self) {
        _size = size;
    }

    return self;
}

- (NSString *)prepareXML {
    return [NSString stringWithFormat:XML_RECTANGLE_FORMAT, self.point.x, self.point.y, self.size.width, self.size.height];
}

- (CGFloat)area {
    return _size.width + _size.height;
}

- (CGSize)shapeSize {
    return _size;
}

@end
